//1
//Класс зоряних війн
class Card {
    
    constructor(usersURL,postsURL){
        this.usersURL = usersURL
        this.postsURL = postsURL

        this.usersArray = []
        this.postsArray = []

        this.container = document.createElement('div')
        document.body.appendChild(this.container)
        this.container.style.display = 'grid'
        this.container.style.gridTemplateColumns = 'repeat(2, 1fr)'
        this.container.style.gap = '20px';

    } 




    cardCreate(person,post) {
        
        let card = document.createElement('div')
        let had = document.createElement('div')
        let name = document.createElement('p')
        let adress = document.createElement('a')
        let title = document.createElement('h1')
        let text = document.createElement('p')
        let delBtn = document.createElement('button')

        card.appendChild(had);
        card.appendChild(title);
        card.appendChild(text);
        card.appendChild(delBtn);
        had.appendChild(name);
        had.appendChild(adress);

        card.style.display = 'flex';
        card.style.flexDirection = 'column';
        card.style.padding = '20px'
        card.style.minWidth = '50%';
        card.style.backgroundColor = 'rgb(0, 0, 51)';
        card.style.borderRadius = '15px';
        card.style.color = 'white';
    
        had.style.display = 'flex';
        had.style.justifyContent = 'space-between'
        had.style.gap = '20px';
        had.style.fontSize = '30px';
    
        adress.style.color = 'gray';
        title.style.color = 'gray';
        text.style.fontSize = '20px';

        delBtn.textContent = 'Видалити пост'
        delBtn.style.color = 'white';
        delBtn.style.borderRadius = '15px';
        delBtn.style.backgroundColor = 'rgb(0, 0, 153)'
        delBtn.style.alignSelf = 'center'

        name.textContent = person.name
        adress.href = person.email
        adress.textContent = '@' + person.username
        title.textContent = post.title
        text.textContent = post.body

        this.container.appendChild(card)
    }




    async arraysAdd() {

        const usersGet = await fetch(this.usersURL)
        const postsGet = await fetch(this.postsURL)

        this.usersArray = await usersGet.json();
        this.postsArray = await postsGet.json();

        
        
        this.usersArray.forEach(thisUser=>{

            this.postsArray.forEach(thisPost=>{
                if(thisUser.id == thisPost.userId) {
                    this.cardCreate(thisUser,thisPost)
                }
            })
        })

        console.log(this.usersArray);
        console.log(this.postsArray);
    }



    del(){
        this.container.addEventListener("click", (event) => {
            // console.log(event.target.parentElement)
    
            if (event.target.innerHTML == 'Видалити пост') {
                let card = event.target.parentElement
                let [div,h1,p,btn] = card.children

                this.postsArray.forEach(element=>{
                    
                    if(element.title == h1.textContent) {
                        fetch(
                            this.postsURL + `/${element.id}`, {
                            method: 'DELETE'
                        })
                        .then((response)=>{
                            if(response.ok) {
                                card.remove()
                            }
                        })
                        .then(console.log("Пост за посиланням " + this.postsURL + `/${element.id}`+' видалено'))
                    }
                })
                
            }
            
        });
    }


}


//Створення 
const HW5 = new Card('https://ajax.test-danit.com/api/json/users','https://ajax.test-danit.com/api/json/posts')

HW5.arraysAdd()
    .then(() => HW5.del())

